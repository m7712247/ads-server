all: release

$(CC) = gcc

debug:
	$(CC) -o advertisementserver -Wall advertisementserver.c

release:
	$(CC) -o advertisementserver -Wall advertisementserver.c

clean:
	rm advertisementserver